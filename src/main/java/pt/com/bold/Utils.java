package pt.com.bold;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class Utils {
	
	
	public static Dataset<Row> getLoanDataFrame(SparkSession spark) {
		Dataset<Row> dataFrame = spark.read().format("csv").option("InferSchema", "true").option("header", "true")
				.option("delimiter", ",").load("in/bank_loan.csv");
		return dataFrame;
	}
	
	public static void printRowsAndColumnNumber(Dataset<Row> dataFrame) {
		System.out.println("Number of Lines: " + dataFrame.count());
		System.out.println("Number of Columns: " + dataFrame.columns().length);
	}

}
