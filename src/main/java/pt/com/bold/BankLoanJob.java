package pt.com.bold;

import static org.apache.spark.sql.functions.col;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class BankLoanJob {

	
	public static final String APP_NAME = "TestBoldCreditAgricole";
	

	public void process() {
		
		SparkSession spark = SparkSession.builder().appName(APP_NAME).master("local[*]")
				.getOrCreate();
		
		// 1. Load into the memory the bank_loan.csv file.
		Dataset<Row> dataFrame = Utils.getLoanDataFrame(spark);
		
		//Data Understanding
		performDataUnderstanding(dataFrame);
		
		//Data Preparation
		dataFrame =  performDataPreparation(dataFrame);
		
		//Spark Partitioning
		performSparkPartitioning(dataFrame);
		
	}
	

	public void performDataUnderstanding(Dataset<Row> dataFrame) {

		// 2. Display into the console the number of rows and the number of columns.
		Utils.printRowsAndColumnNumber(dataFrame);

		// 3. Display into the console the first rows of the bank_loan data.
		
		//3.1 Only first ROW
		System.out.println("First Row: " + dataFrame.first());

		// 3.2 Display first ROWS (3)
		dataFrame.show(3);

		// 4. Display into the console all possible values of Loan.Status
		Dataset<Row> distinctLoanStatus = dataFrame.select("Loan Status").distinct();
		distinctLoanStatus.show();

	}

	public Dataset<Row> performDataPreparation(Dataset<Row> dataFrame) {

		Dataset<Row> deletedColsDF = dataFrame.drop("Loan ID", "Customer ID");

		/*
		 * 2. Filter the dataset and keep only rows that have: a. Open.Credit<10000 b.
		 * Annual.Income<7000000
		 */
		Dataset<Row> filteredDF = deletedColsDF.filter(col("Maximum Open Credit").lt(10000))
				.filter(col("Annual Income").lt(7000000));
		filteredDF.show(10);

		// 3. Add a new filter and keep only data that have Current.Loan.Amount<20000000
		Dataset<Row> newFilterDF = filteredDF.filter(col("Current Loan Amount").lt(20000000));

		// 4. For each row, if at least a column has a missing value, remove the corresponding row.
		Dataset<Row> columnsNotNullDF = newFilterDF.na().drop();
		
		// 5.	Display into the console the number of rows and the number of columns of the resulting dataset.
		Utils.printRowsAndColumnNumber(columnsNotNullDF);

		// 6. Split the resulting dataset into two persistent datasets:

		// 6.a. One dataset containing only columns with nominal values.
		Dataset<Row> numericColumnsDF = columnsNotNullDF.select("Current Loan Amount", "Credit Score", "Annual Income",
				"Monthly Debt", "Years of Credit History", "Number of Open Accounts", "Number of Credit Problems",
				"Current Credit Balance", "Maximum Open Credit", "Bankruptcies", "Tax Liens");
		
		
		// 6.b. One dataset containing only columns with numerical values.
		Dataset<Row> nominalColumnsDF = columnsNotNullDF.select("Loan Status","Term","Years in current job",
				"Home Ownership","Purpose","Months since last delinquent");
		

		return nominalColumnsDF;
	}
	
	public void performSparkPartitioning(Dataset<Row> dataFrame) {
		
		// 1.	Partition the dataset based on Loan.Status column values. 
		dataFrame.repartition(col("Loan Status"));
		
		// 2.	Write each partition data into the console.
		dataFrame.toJavaRDD().foreachPartition(it -> it.forEachRemaining(System.out::println));
		
		
	}

}
